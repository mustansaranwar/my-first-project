package nz.co.twg.goldenpath.javarestapi.model;

import java.util.ArrayList;
import java.util.List;

public class UserListResponse {
    List<User> data = new ArrayList<>();

    public List<User> getData() {
        return data;
    }
}
