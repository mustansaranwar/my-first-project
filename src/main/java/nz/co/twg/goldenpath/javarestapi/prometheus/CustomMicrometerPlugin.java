package nz.co.twg.goldenpath.javarestapi.prometheus;

import io.javalin.Javalin;
import io.javalin.core.plugin.Plugin;
import io.javalin.http.*;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.binder.http.DefaultHttpServletRequestTagsProvider;
import io.micrometer.core.instrument.binder.jetty.JettyConnectionMetrics;
import io.micrometer.core.instrument.binder.jetty.JettyServerThreadPoolMetrics;
import io.micrometer.core.instrument.binder.jetty.TimedHandler;
import nz.co.twg.goldenpath.javarestapi.App;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class CustomMicrometerPlugin implements Plugin {

    private static final Logger LOG = LoggerFactory.getLogger(CustomMicrometerPlugin.class);

    private static final String EXCEPTION_HEADER = "__micrometer_exception_name";

    private final MeterRegistry registry;
    private final Iterable<Tag> tags;

    public static ExceptionHandler<Exception> EXCEPTION_HANDLER = (e, ctx) -> {
        String simpleName = e.getClass().getSimpleName();
        ctx.header(EXCEPTION_HEADER, StringUtils.isNotBlank(simpleName) ? simpleName : e.getClass().getName());
        ctx.status(500);
    };

    public CustomMicrometerPlugin(MeterRegistry registry) {
        this(registry, Tags.empty());
    }

    public CustomMicrometerPlugin(MeterRegistry registry, Iterable<Tag> tags) {
        this.registry = registry;
        this.tags = tags;
    }

    @Override
    public void apply(Javalin app) {
        Server server = app.server().server();

        app.exception(Exception.class, EXCEPTION_HANDLER);

        server.insertHandler(new TimedHandler(registry, tags, new DefaultHttpServletRequestTagsProvider() {
            @Override
            public Iterable<Tag> getTags(HttpServletRequest request, HttpServletResponse response) {
                String exceptionName = response.getHeader(EXCEPTION_HEADER);
                response.setHeader(EXCEPTION_HEADER, null);

                String pathInfo = request.getPathInfo().replace(app.config.contextPath, "");

                String uri = app.servlet().getMatcher()
                        .findEntries(HandlerType.valueOf(request.getMethod()), pathInfo)
                        .stream()
                        .findAny()
                        .map(HandlerEntry::getPath)
                        .map(path -> path.equals(app.config.contextPath) || StringUtils.isBlank(path) ? "root" : path)
                        .map(path -> response.getStatus() >= 300 && response.getStatus() < 400 ? "REDIRECTION" : path)
                        .map(path -> response.getStatus() == 404 ? "NOT_FOUND" : path)
                        .orElse("unknown");

                return Tags.concat(
                        super.getTags(request, response),
                        "uri", uri,
                        "exception", exceptionName == null ? "None" : exceptionName
                );
            }
        }));

        new JettyServerThreadPoolMetrics(server.getThreadPool(), tags).bindTo(registry);
        new JettyConnectionMetrics(registry, tags);
    }
}
