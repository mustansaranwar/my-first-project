package nz.co.twg.goldenpath.javarestapi;

import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.dsl.OpenApiBuilder;
import io.javalin.plugin.openapi.dsl.OpenApiDocumentation;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import io.opentelemetry.OpenTelemetry;
import io.opentelemetry.exporters.logging.LoggingSpanExporter;
import io.opentelemetry.exporters.zipkin.ZipkinSpanExporter;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.trace.TracerSdkManagement;
import io.opentelemetry.sdk.trace.export.SimpleSpanProcessor;
import io.opentelemetry.trace.Span;
import io.opentelemetry.trace.Tracer;
import io.prometheus.client.exporter.HTTPServer;
import io.swagger.v3.oas.models.info.Info;
import nz.co.twg.goldenpath.javarestapi.model.User;
import nz.co.twg.goldenpath.javarestapi.model.UserListResponse;
import nz.co.twg.goldenpath.javarestapi.prometheus.CustomMicrometerPlugin;
import nz.co.twg.goldenpath.javarestapi.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Application
 */
public class App {
    private static final Logger LOG = LoggerFactory.getLogger(App.class);

    private static final String CONTEXT_PATH = "/do-goldenpath-java-rest-api";
    private static final Tracer TRACER = OpenTelemetry.getTracer("nz.co.twg.goldenpath.javarestapi");
    private static final LoggingSpanExporter LOGGING_EXPORTER = new LoggingSpanExporter();
    private static AppConfig appConfig;

    private static PrometheusMeterRegistry prometheusRegistry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);

    public static void main(String[] args) throws IOException {
        LOG.info("Starting application");
        appConfig = AppConfig.getConfig();

        initializePrometheus();

        Javalin app = Javalin.create(config -> {
            config.registerPlugin(new OpenApiPlugin(getOpenApiOptions()));
            config.contextPath = CONTEXT_PATH;
            config.registerPlugin(new CustomMicrometerPlugin(prometheusRegistry));
        }).start(appConfig.getApplicationPort());


        app.before(ctx -> {
            beforeEveryRequest(ctx);
        });

        app.after(ctx -> {
            afterEveryRequest(ctx);
        });

        OpenApiDocumentation getUserDocumentation = OpenApiBuilder.document()
                .json("200", User.class);
        app.get("/v1/user/:name", OpenApiBuilder.documented(getUserDocumentation, ctx -> {
            ctx.status(200);
            ctx.json(new User(ctx.pathParam("name")));
        }));

        OpenApiDocumentation postUserDocumentation = OpenApiBuilder.document()
                .body(User.class)
                .json("201", User.class);
        app.post("/v1/user", OpenApiBuilder.documented(postUserDocumentation, ctx -> {
            ctx.status(201);
        }));

        OpenApiDocumentation getUsersDocumentation = OpenApiBuilder.document()
                .json("200", UserListResponse.class);
        app.get("/v1/users", OpenApiBuilder.documented(getUsersDocumentation, ctx -> {
            ctx.json(new UserService().getUsers());
        }));

        // OpenTelemetry
        TracerSdkManagement tracerManagement = OpenTelemetrySdk.getTracerManagement();

        if (appConfig.isOpentelemetryZipkinEnabled()) {
            ZipkinSpanExporter zipkinSpanExporter = ZipkinSpanExporter.newBuilder()
                    .setEndpoint(appConfig.getOpentelemetryZipkinHost() + "/api/v2/spans")
                    .setServiceName(appConfig.getApplicationName())
                    .build();
            tracerManagement.addSpanProcessor(SimpleSpanProcessor.newBuilder(zipkinSpanExporter).build());
        }

        if (appConfig.isOpentelemetryConsoleEnabled()) {
            // set to export the traces also to a console
            tracerManagement.addSpanProcessor(SimpleSpanProcessor.newBuilder(LOGGING_EXPORTER).build());
        }
        LOG.info("Application up and running!");
        LOG.info("Check out Swagger UI docs at http://localhost:{}{}/swagger-ui", appConfig.getApplicationPort(), CONTEXT_PATH);
        LOG.info("Check out Prometheus stats at http://localhost:{}", appConfig.getPrometheusPort());
    }

    private static OpenApiOptions getOpenApiOptions() {
        Info applicationInfo = new Info()
                .version("1.0")
                .description("My Application");

        OpenApiOptions openApiOptions = new OpenApiOptions(applicationInfo)
                .path("/swagger.json")
                .swagger(new SwaggerOptions("/swagger-ui").title("Swagger Documentation"));

        return openApiOptions;
    }

    private static void initializePrometheus() throws IOException {
        HTTPServer prometheusServer = new HTTPServer(new InetSocketAddress(8050), prometheusRegistry.getPrometheusRegistry());
    }

    private static void beforeEveryRequest(Context ctx) {
        if (ctx.path().startsWith("/swagger")) {
            return;
        }

        Span span =
                TRACER.spanBuilder("/").setSpanKind(Span.Kind.SERVER).startSpan();

        span.setAttribute("component", "http");
        span.setAttribute("http.method", ctx.method());
        span.setAttribute("http.scheme", ctx.scheme());
        span.setAttribute("http.host", ctx.host());
        span.setAttribute("http.target", ctx.path());
        ctx.attribute("span", span);
    }

    private static void afterEveryRequest(Context ctx) {
        if (ctx.path().startsWith("/swagger")) {
            return;
        }
        Span span = ctx.attribute("span");
        span.end();
    }
}
