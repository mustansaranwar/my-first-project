package nz.co.twg.goldenpath.javarestapi.service;

import nz.co.twg.goldenpath.javarestapi.model.User;
import nz.co.twg.goldenpath.javarestapi.model.UserListResponse;

public class UserService {

    public UserListResponse getUsers() {

        UserListResponse userList = new UserListResponse();
        userList.getData().add(new User("User1"));
        userList.getData().add(new User("User2"));

        return userList;
    }
}
