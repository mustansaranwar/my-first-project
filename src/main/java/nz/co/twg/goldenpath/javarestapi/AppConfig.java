package nz.co.twg.goldenpath.javarestapi;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class AppConfig {
    public static AppConfig getConfig() {
        Config conf = ConfigFactory.load();

        AppConfig appConfig = new AppConfig();

        appConfig.setApplicationName(conf.getString("application.name"));
        appConfig.setApplicationPort(conf.getInt("application.port"));
        appConfig.setPrometheusPort(conf.getInt("prometheus.port"));
        appConfig.setOpentelemetryConsoleEnabled(conf.getBoolean("opentelemetry.console.enabled"));
        appConfig.setOpentelemetryZipkinEnabled(conf.getBoolean("opentelemetry.zipkin.enabled"));
        appConfig.setOpentelemetryZipkinHost(conf.getString("opentelemetry.zipkin.host"));


        return appConfig;
    }

    private String applicationName;
    private int applicationPort;
    private int prometheusPort;
    private boolean opentelemetryConsoleEnabled;
    private boolean opentelemetryZipkinEnabled;
    private String opentelemetryZipkinHost;

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public int getApplicationPort() {
        return applicationPort;
    }

    public void setApplicationPort(int applicationPort) {
        this.applicationPort = applicationPort;
    }

    public int getPrometheusPort() {
        return prometheusPort;
    }

    public void setPrometheusPort(int prometheusPort) {
        this.prometheusPort = prometheusPort;
    }

    public boolean isOpentelemetryConsoleEnabled() {
        return opentelemetryConsoleEnabled;
    }

    public void setOpentelemetryConsoleEnabled(boolean opentelemetryConsoleEnabled) {
        this.opentelemetryConsoleEnabled = opentelemetryConsoleEnabled;
    }

    public boolean isOpentelemetryZipkinEnabled() {
        return opentelemetryZipkinEnabled;
    }

    public void setOpentelemetryZipkinEnabled(boolean opentelemetryZipkinEnabled) {
        this.opentelemetryZipkinEnabled = opentelemetryZipkinEnabled;
    }

    public String getOpentelemetryZipkinHost() {
        return opentelemetryZipkinHost;
    }

    public void setOpentelemetryZipkinHost(String opentelemetryZipkinHost) {
        this.opentelemetryZipkinHost = opentelemetryZipkinHost;
    }
}
