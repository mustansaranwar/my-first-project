FROM openjdk:11.0.9.1-jre
WORKDIR /app
COPY target/do-goldenpath-java-rest-api-1.0.jar ./

# Some updates to dockerfile

CMD ["java", "-jar", "do-goldenpath-java-rest-api-1.0.jar"]
